package pl.rojek.tomasz;

/**
 * Created by Sławek on 2017-08-18.
 */
public enum Sex {
    MALE, FEMALE;
}
