package pl.rojek.tomasz;

import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class Bird extends Animal{
    public Bird(int age, String name, TypeOfAnimals type, int numberOfLimb, List<Integer> allergies, List<KindOfFeed> kindOffFeeds) {
        super(age, name, type, numberOfLimb, allergies, kindOffFeeds);
    }
}
