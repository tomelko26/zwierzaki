package pl.rojek.tomasz;

import java.util.Comparator;
import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public class KindOfFeed implements Comparable<KindOfFeed> {


    int noOf;
    Feed feed; //pokarm który dajemy zwierzęciu
    int quantity;

    public int quantity(){//ile jednostek posiada ten pokarm
        this.quantity= quantity;
    }

    public int getNoOf() {
        return noOf;
    }

    public KindOfFeed(int noOf) {
        this.noOf = noOf;
    }


    public KindOfFeed(Feed feed, int quantity){
        this.noOf = noOf;
        this.feed = feed;
        this.quantity = quantity;
    }



    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    @Override //nadpisuje
    public int compareTo(KindOfFeed o) {
        int result = -1;

        if (getFeed() == o.getFeed()) {
            result = 0;
        }
        return result;
    }


    public boolean equals(KindOfFeed obj) {
        //tu jest przeciążenie, dodanie na KindOfFeed możemy wywołać już dwa
        //

       // return super.equals(obj); //super wskazuje na instancje z nadrzędnej z której dziedziczymy
        boolean result = false;


        if (obj instanceof KindOfFeed) {
            KindOfFeed kindOfFeed = (KindOfFeed) obj;

        if (getFeed() == kindOfFeed.getFeed()) {
            return true;
        }

        }
        return result;
    }


}
