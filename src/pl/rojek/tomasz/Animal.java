package pl.rojek.tomasz;

import java.util.List;

/**
 * Created by RENT on 2017-08-11.
 */
public abstract class Animal  {
    private int age;
    private String name;
    private TypeOfAnimals type;
    private int numberOfLimb;
    private List<Integer> allergies;
    private List<KindOfFeed> kindOffFeeds;


    List<Integer> allergy;

    List <KindOfFeed> kindOfFeeds;

    public Animal(int age, String name, TypeOfAnimals type, int numberOfLimb, List<Integer> allergies, List<KindOfFeed> kindOffFeeds) {
        this.age = age;
        this.name = name;
        this.type = type;
        this.numberOfLimb = numberOfLimb;
        this.allergies = allergies;
        this.kindOffFeeds = kindOffFeeds;
    }


    public List<Integer> getAllergy() {
        return allergy;
    }

    public void setAllergy(List<Integer> allergy) {
        this.allergy = allergy;
    }


    public boolean feedMe (KindOfFeed kindOfFeed){
        return false;
    }

    public List<KindOfFeed> getFeed(){
        return kindOfFeeds;

    }

}
